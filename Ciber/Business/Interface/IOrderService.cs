﻿using Ciber.Common.Helpers;
using Ciber.Models;
using Ciber.Models.Request;

namespace Ciber.Business.Interface
{
    public interface IOrderService
    {
        Response<OrderModel> Save(OrderModel model, string createdBy);

        Response<OrderModel> Get(int id);

        Response<List<OrderModel>> List(SearchModelParam<FilterProperty> searchModelParam);

    }
}