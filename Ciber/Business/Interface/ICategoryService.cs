﻿using Ciber.Common.Helpers;
using Ciber.Models.Request;

namespace Ciber.Business.Interface
{
    public interface ICategoryService
    {
        Response<CategoryModel> Save(CategoryModel model, string createdBy);

        Response<CategoryModel> Update(CategoryModel model, string updatedBy);

        Response<CategoryModel> Get(int id);

        Response<CategoryModel> Delete(int id, string updatedBy);

        Response<List<CategoryModel>> List(SearchModelParam<FilterProperty> searchModelParam);

        Response<List<CategoryResponse>> ListAll();

    }
}