﻿using Ciber.Common.Helpers;
using Ciber.Models.Request;

namespace Ciber.Business.Interface
{
    public interface IProductService
    {
        Response<List<ProductResponse>> ListAll(int categoryId);

    }
}