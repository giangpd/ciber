﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using Ciber.Business.Interface;
using Ciber.Common.Helpers;
using Ciber.Infrastructure.Entity;
using Ciber.Models;
using Ciber.Repositories.Interface;
using static Ciber.Common.Constants;
using static Ciber.Common.Helpers.AppHelper;
using CategoryModel = Ciber.Models.Request.CategoryModel;
using Ciber.Extensions;
using Ciber.Models.Request;
using Microsoft.EntityFrameworkCore;
using Abp.Extensions;
using Ciber.Repositories.Implement;

namespace Ciber.Business.Implement
{
    public class CategoryService : BaseService, ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(IUnitOfWork unitOfWork, ICategoryRepository categoryRepository, IMapper mapper, UserManager<IdentityUser> userManager) : base(unitOfWork, mapper, userManager)
        {
            _categoryRepository = categoryRepository;
        }

        public Response<CategoryModel> Save(CategoryModel model, string createdBy)
        {
            try
            {
                _unitOfWork.BeginTransaction();

                var result = _mapper.Map<Category>(model);
                result.CreatedBy = createdBy;
                _categoryRepository.Add(result);

                _unitOfWork.SaveChanges();
                _unitOfWork.Commit();

                return new Response<CategoryModel>(model, true, $"Category {ApiResponseMessage.CreateOk}", StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                return new Response<CategoryModel>(false, ex.Message, StatusCodes.Status400BadRequest);
            }
        }

        public Response<CategoryModel> Update(CategoryModel model, string updatedBy)
        {
            try
            {
                _unitOfWork.BeginTransaction();

                var category = _categoryRepository
                    .Queryable().Where(x => x.Id == model.Id && x.IsDelete == false && x.IsActive == true).FirstOrDefault();
                if (category.IsNull()) return new Response<CategoryModel>(model, false, $"Category {ApiResponseMessage.NotFound}", StatusCodes.Status400BadRequest);
                {
                    category.Name = model.Name;
                    category.Description = model.Description;
                    category.UpdatedBy = updatedBy;
                    _categoryRepository.Update(category);
                }

                _unitOfWork.SaveChanges();
                _unitOfWork.Commit();

                return new Response<CategoryModel>(model, true, $"Category {ApiResponseMessage.UpdateOk}", StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                return new Response<CategoryModel>(false, ex.Message, StatusCodes.Status400BadRequest);
            }
        }

        public Response<CategoryModel> Get(int id)
        {
            try
            {
                var category = _categoryRepository
                    .AsNoTracking().Where(x => x.Id == id && x.IsDelete == false && x.IsActive == true).FirstOrDefault();
                if (category.IsNull()) return new Response<CategoryModel>(false, $"Category {ApiResponseMessage.NotFound}", StatusCodes.Status400BadRequest);

                var result = _mapper.Map<CategoryModel>(category);

                return new Response<CategoryModel>(result, true, StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                return new Response<CategoryModel>(false, ex.Message, StatusCodes.Status400BadRequest);
            }
        }

        public Response<CategoryModel> Delete(int id, string updatedBy)
        {
            try
            {
                _unitOfWork.BeginTransaction();

                var category = _categoryRepository
                    .Queryable().Where(x => x.Id == id && x.IsDelete == false && x.IsActive == true).FirstOrDefault();
                if (category.IsNull()) return new Response<CategoryModel>(false, $"Category {ApiResponseMessage.NotFound}", StatusCodes.Status400BadRequest);
                {
                    category.IsDelete = true;
                    category.UpdatedBy = updatedBy;
                    _categoryRepository.Update(category);
                }

                _unitOfWork.SaveChanges();
                _unitOfWork.Commit();

                return new Response<CategoryModel>(true, $"Category {ApiResponseMessage.DeleteOk}", StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                return new Response<CategoryModel>(false, ex.Message, StatusCodes.Status400BadRequest);
            }
        }

        public Response<List<CategoryModel>> List(SearchModelParam<FilterProperty> searchModelParam)
        {
            try
            {
                var list = _categoryRepository.AsNoTracking().Where(x => x.IsDelete == false && x.IsActive == true);
                var totalCount = list.Count();

                IQueryable<Category> query;

                query = list.PageBy(searchModelParam).OrderBy(searchModelParam);

                var nameProperty = searchModelParam.Filter.NameProperty ?? "";
                var valueProperty = searchModelParam.Filter.ValueProperty ?? "";
                if (searchModelParam.Filter != null && !nameProperty.IsNullOrEmpty() && !valueProperty.IsNullOrEmpty())
                {
                    query = query.Contains(nameProperty, valueProperty);
                }

                var result = _mapper.Map<List<CategoryModel>>(query.ToList());

                return new Response<List<CategoryModel>>(result, true, totalCount, StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                return new Response<List<CategoryModel>>(false, ex.Message, StatusCodes.Status400BadRequest);
            }
        }

        public Response<List<CategoryResponse>> ListAll()
        {
            try
            {
                var list = _categoryRepository.AsNoTracking()
                    .Where(x => x.IsDelete == false && x.IsActive == true).ToList();
                var totalCount = list.Count();

                var result = _mapper.Map<List<CategoryResponse>>(list);

                return new Response<List<CategoryResponse>>(result, true, totalCount, StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                return new Response<List<CategoryResponse>>(false, ex.Message, StatusCodes.Status400BadRequest);
            }
        }
    }
}