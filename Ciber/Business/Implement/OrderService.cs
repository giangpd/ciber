﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using Ciber.Business.Interface;
using Ciber.Common.Helpers;
using Ciber.Infrastructure.Entity;
using Ciber.Models;
using Ciber.Repositories.Interface;
using static Ciber.Common.Constants;
using static Ciber.Common.Helpers.AppHelper;
using OrderModel = Ciber.Models.Request.OrderModel;
using Ciber.Extensions;
using Ciber.Models.Request;
using Microsoft.EntityFrameworkCore;
using Abp.Extensions;

namespace Ciber.Business.Implement
{
    public class OrderService : BaseService, IOrderService
    {
        private readonly IOrderRepository _OrderRepository;

        public OrderService(IUnitOfWork unitOfWork, IOrderRepository orderRepository, IMapper mapper, UserManager<IdentityUser> userManager) : base(unitOfWork, mapper, userManager)
        {
            _OrderRepository = orderRepository;
        }

        public Response<OrderModel> Save(OrderModel model, string createdBy)
        {
            try
            {
                _unitOfWork.BeginTransaction();

                var result = _mapper.Map<Order>(model);
                result.CreatedBy = createdBy;
                _OrderRepository.Add(result);

                _unitOfWork.SaveChanges();
                _unitOfWork.Commit();

                return new Response<OrderModel>(model, true, $"Order {ApiResponseMessage.CreateOk}", StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                return new Response<OrderModel>(false, ex.Message, StatusCodes.Status400BadRequest);
            }
        }

        public Response<OrderModel> Get(int id)
        {
            try
            {
                var Order = _OrderRepository
                    .AsNoTracking().Where(x => x.Id == id && x.IsDelete == false && x.IsActive == true).FirstOrDefault();
                if (Order.IsNull()) return new Response<OrderModel>(false, $"Order {ApiResponseMessage.NotFound}", StatusCodes.Status400BadRequest);

                var result = _mapper.Map<OrderModel>(Order);

                return new Response<OrderModel>(result, true, StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                return new Response<OrderModel>(false, ex.Message, StatusCodes.Status400BadRequest);
            }
        }

        public Response<List<OrderModel>> List(SearchModelParam<FilterProperty> searchModelParam)
        {
            try
            {
                var list = _OrderRepository.AsNoTracking().Where(x => x.IsDelete == false && x.IsActive == true);
                var totalCount = list.Count();

                IQueryable<Order> query;

                query = list.PageBy(searchModelParam).OrderBy(searchModelParam);

                var nameProperty = searchModelParam?.Filter?.NameProperty ?? "";
                var valueProperty = searchModelParam?.Filter?.ValueProperty ?? "";
                if (searchModelParam.Filter != null && !nameProperty.IsNullOrEmpty() && !valueProperty.IsNullOrEmpty())
                {
                    query = query.Contains(nameProperty, valueProperty);
                }

                var result = _mapper.Map<List<OrderModel>>(query.ToList());

                return new Response<List<OrderModel>>(result, true, totalCount, StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                return new Response<List<OrderModel>>(false, ex.Message, StatusCodes.Status400BadRequest);
            }
        }
    }
}