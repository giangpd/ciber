﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using Ciber.Business.Interface;
using Ciber.Common.Helpers;
using Ciber.Infrastructure.Entity;
using Ciber.Models;
using Ciber.Repositories.Interface;
using static Ciber.Common.Constants;
using static Ciber.Common.Helpers.AppHelper;
using Ciber.Extensions;
using Microsoft.EntityFrameworkCore;
using Abp.Extensions;
using Ciber.Models.Request;

namespace Ciber.Business.Implement
{
    public class ProductService : BaseService, IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IUnitOfWork unitOfWork, IProductRepository productRepository, IMapper mapper, UserManager<IdentityUser> userManager) : base(unitOfWork, mapper, userManager)
        {
            _productRepository = productRepository;
        }

        public Response<List<ProductResponse>> ListAll(int categoryId)
        {
            try
            {
                var list = _productRepository.AsNoTracking()
                    .Where(x => x.IsDelete == false && x.IsActive == true && x.CategoryId == categoryId).ToList();
                var totalCount = list.Count();

                var result = _mapper.Map<List<ProductResponse>>(list);

                return new Response<List<ProductResponse>>(result, true, totalCount, StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                return new Response<List<ProductResponse>>(false, ex.Message, StatusCodes.Status400BadRequest);
            }
        }
    }
}