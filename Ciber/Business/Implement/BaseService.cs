﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Ciber.Infrastructure;
using Ciber.Repositories.Interface;

namespace Ciber.Business.Implement
{
    public class BaseService
    {
        public readonly IMapper _mapper;
        public readonly IUnitOfWork _unitOfWork;
        public readonly UserManager<IdentityUser> _userManager;

        public readonly ApplicationDbContext _context;

        public BaseService()
        {
        }

        public BaseService(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        public BaseService(IMapper mapper)
        {
            _mapper = mapper;
        }

        public BaseService(IUnitOfWork unitOfWork, IMapper mapper, UserManager<IdentityUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
        }

        public BaseService(IMapper mapper, UserManager<IdentityUser> userManager, ApplicationDbContext context)
        {
            _mapper = mapper;
            _userManager = userManager;
            _context = context;
        }

        public BaseService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}