﻿namespace Ciber.Models.Request
{
    public class OrderModel
    {
        public OrderModel(int id, int customerId, string customerName, int productId, string productName, int categoryId, string categoryName, int amount, DateTime orderDate)
        {
            Id = id;
            CustomerId = customerId;
            CustomerName = customerName;
            ProductId = productId;
            ProductName = productName;
            CategoryId = categoryId;
            CategoryName = categoryName;
            Amount = amount;
            OrderDate = orderDate;
        }

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int Amount { get; set; }
        public DateTime OrderDate { get; set; }
    }
}