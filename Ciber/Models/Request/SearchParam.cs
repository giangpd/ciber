﻿using Ciber.Common.Helpers;

namespace Ciber.Models.Request
{
    public class SearchModelParam<T> : PagedAndSortedRequest, IPagedAndSortedRequest
    {
        public SearchModelParam()
        {
            
        }
        public SearchModelParam(int pIndex, int pSize, string sortBy, string sortDirection, T filter) : base(pIndex, pSize, sortBy, sortDirection)
        {
            Filter = filter;
        }

        public T Filter { get; set; }
    }

    public class SearchTexParam : PagedAndSortedRequest, IPagedAndSortedRequest
    {
        public string Text { get; set; }

        public SearchTexParam(int pIndex, int pSize, string sortBy, string sortDirection, string text) : base(pIndex, pSize, sortBy, sortDirection)
        {
            Text = text;
        }
    }

    public class FilterProperty
    {
        public string? NameProperty { get; set; }
        public string? ValueProperty { get; set; }
    }
}