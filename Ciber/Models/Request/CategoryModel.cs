﻿namespace Ciber.Models.Request
{
    public class CategoryModel
    {
        public CategoryModel(int id, string name, string description)
        {
            Id = id;
            Name = name;
            Description = description;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
    public class CategoryResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}