﻿using System.Text.RegularExpressions;

namespace Ciber.Extensions
{
    public static class StringExtension
    {
        public static string RemoveUnderscore(this string s)
        {
            if (string.IsNullOrEmpty(s)) return s;
            return s.Replace("_", string.Empty);
        }

        public static string FormatNumber(string sNumber, string sperator = ".")
        {
            var num = 3;
            var num2 = 0;
            for (var i = 1; i <= sNumber.Length / 3; i++)
            {
                if (num + num2 < sNumber.Length) sNumber = sNumber.Insert(sNumber.Length - num - num2, sperator);
                num += 3;
                num2++;
            }

            return sNumber;
        }

        public static List<string>? SplitNearestSpace(this string str, int partLength)
        {
            if (string.IsNullOrEmpty(str))
                return null;

            var parts = new List<string>();
            string part = string.Empty;
            string[] words = str.Trim().Split(' ');
            foreach (var word in words)
            {
                if (part.Length + word.Length < partLength)
                    part += string.IsNullOrEmpty(part) ? word : " " + word;
                else
                {
                    parts.Add(part);
                    part = word;
                }
            }
            parts.Add(part);

            return parts;
        }

        public static bool HasHtmlTags(this string? str)
        {
            return !string.IsNullOrEmpty(str) && Regex.IsMatch(str, "<(.|\n)*?>");
        }

        public static string FormatName(this string str)
        {
            if (string.IsNullOrEmpty(str) || string.IsNullOrEmpty(str.Trim())) return str;
            str = str.RemoveMultiSpaces();
            var array = str.Split(' ');
            var capitalizeArray = array.Select(item => Capitalize(item)).ToList();
            return String.Join(" ", capitalizeArray);
        }

        public static string RemoveMultiSpaces(this string str)
        {
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"[ ]{2,}", options);
            str = regex.Replace(str, " ");
            return str;
        }

        public static string Capitalize(string str)
        {
            str = str.Trim();
            if (str.Length > 0)
            {
                var first = str[0].ToString();
                var rest = str[1..];
                return first.ToUpper() + rest.ToLower();
            }
            return str;
        }

        public static string FullFormatPolNum(this string polNum)
        {
            if (string.IsNullOrEmpty(polNum) || string.IsNullOrEmpty(polNum.Trim())) return polNum;
            polNum = polNum.RemoveMultiSpaces();
            return polNum.PadLeft(17, '0');
        }
    }
}