﻿using Microsoft.EntityFrameworkCore;
using Ciber.Extensions;
using Ciber.Infrastructure;
using Ciber.Repositories.Interface;

namespace Ciber.Repositories.Implement
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected ApplicationDbContext _context;
        private DbSet<T> dbSet = null;
        private DbSet<T> _dbSet;
        protected DbSet<T> DbSet
        {
            get
            {
                if (_dbSet == null)
                {
                    _dbSet = _context.Set<T>();
                }

                return _dbSet;
            }
        }
        public Repository(ApplicationDbContext context)
        {
            _context = context;
            dbSet = _context.Set<T>();
        }
        public T Add(T entity)
        {
            entity.TrySetProperty("CreatedDate", DateTime.UtcNow);
            entity.TrySetProperty("UpdatedDate", DateTime.UtcNow);

            DbSet.Add(entity);
            return entity;
        }

        public bool SoftDelete(T entity)
        {
            entity.TrySetProperty("UpdatedDate", DateTime.UtcNow);
            entity.TrySetProperty("IsDelete", true);

            DbSet.Update(entity);
            return true;
        }

        public T Update(T entity)
        {
            entity.TrySetProperty("UpdatedDate", DateTime.UtcNow);

            DbSet.Update(entity);
            return entity;
        }

        public T Get(object id)
        {
            return DbSet.Find(id);
        }

        public async Task<T> GetAsync(object id)
        {
            return await DbSet.FindAsync(id);
        }

        // Get all data with IsDelete = false or not exist IsDelete column
        public IQueryable<T> Queryable() => DbSet.AsQueryable().WhereExistsOrAll("IsDelete", false);
        // Get all data
        public IQueryable<T> QueryableAll() => DbSet.AsQueryable();
        // Get all data with IsDelete = false or not exist IsDelete column
        public IQueryable<T> AsNoTracking() => DbSet.AsNoTracking().WhereExistsOrAll("IsDelete", false);
        // Get all data
        public IQueryable<T> AsNoTrackingAll() => DbSet.AsNoTracking();
        public IList<T> AddRange(IList<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.TrySetProperty("CreatedDate", DateTime.UtcNow);
                entity.TrySetProperty("UpdatedDate", DateTime.UtcNow);
            }

            DbSet.AddRange(entities);
            return entities;
        }

        public IList<T> UpdateRange(IList<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.TrySetProperty("UpdatedDate", DateTime.UtcNow);
            }

            DbSet.UpdateRange(entities);
            return entities;
        }

        // Update IsDelete = true
        public bool SoftDeleteRange(IList<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.TrySetProperty("UpdatedDate", DateTime.UtcNow);
                entity.TrySetProperty("IsDelete", true);
            }

            DbSet.UpdateRange(entities);
            return true;
        }

        // hard delete data
        public bool Delete(T entity)
        {
            DbSet.Remove(entity);
            return true;
        }
        // hard delete data
        public bool DeleteRange(IList<T> entities)
        {
            DbSet.RemoveRange(entities);
            return true;
        }

        public int SaveChange()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveChangeAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}