﻿using Ciber.Infrastructure;
using Ciber.Infrastructure.Entity;
using Ciber.Repositories.Interface;

namespace Ciber.Repositories.Implement
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}