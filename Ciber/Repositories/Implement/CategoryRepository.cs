﻿using Ciber.Infrastructure;
using Ciber.Infrastructure.Entity;
using Ciber.Repositories.Interface;

namespace Ciber.Repositories.Implement
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}