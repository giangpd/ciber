﻿using Microsoft.AspNetCore.Identity;
using Ciber.Infrastructure;
using Ciber.Repositories.Interface;

namespace Ciber.Repositories.Implement
{
    public class UserRepository : Repository<IdentityUser>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {
        }

        public IdentityUser GetById(string id)
        {
            return DbSet.FirstOrDefault(x => x.Id == id);
        }
    }
}