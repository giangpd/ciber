﻿using Ciber.Infrastructure;
using Ciber.Infrastructure.Entity;
using Ciber.Repositories.Interface;

namespace Ciber.Repositories.Implement
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}