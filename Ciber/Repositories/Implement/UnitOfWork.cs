﻿using Ciber.Infrastructure;
using Ciber.Repositories.Interface;

namespace Ciber.Repositories.Implement
{
    public class UnitOfWork : IUnitOfWork
    {
        public ApplicationDbContext Context { get; }

        public UnitOfWork(ApplicationDbContext context)
        {
            Context = context;
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public void BeginTransaction()
        {
            if (Context.Database.CurrentTransaction != null)
            {
                return;
            }
            Context.Database.BeginTransaction();
        }

        public void Commit()
        {
            if (Context.Database.CurrentTransaction == null)
            {
                return;
            }
            Context.Database.CommitTransaction();
        }

        public void Rollback()
        {
            if (Context.Database.CurrentTransaction == null)
            {
                return;
            }
            Context.Database.RollbackTransaction();
        }
    }
}