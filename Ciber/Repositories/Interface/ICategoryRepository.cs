﻿using Ciber.Infrastructure.Entity;

namespace Ciber.Repositories.Interface
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}