﻿using Microsoft.AspNetCore.Identity;

namespace Ciber.Repositories.Interface
{
    public interface IUserRepository : IRepository<IdentityUser>
    {
        IdentityUser GetById(string id);
    }
}