﻿using Ciber.Infrastructure.Entity;

namespace Ciber.Repositories.Interface
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}