﻿using Ciber.Infrastructure.Entity;

namespace Ciber.Repositories.Interface
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}