﻿using Ciber.Infrastructure;

namespace Ciber.Repositories.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationDbContext Context { get; }

        void SaveChanges();

        void BeginTransaction();

        void Commit();

        void Rollback();
    }
}