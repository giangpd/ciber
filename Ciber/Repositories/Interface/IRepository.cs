﻿namespace Ciber.Repositories.Interface
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Queryable();

        IQueryable<T> QueryableAll();

        IQueryable<T> AsNoTracking();

        IQueryable<T> AsNoTrackingAll();

        T Get(object id);

        Task<T> GetAsync(object id);

        T Add(T entity);

        T Update(T entity);

        IList<T> AddRange(IList<T> entities);

        IList<T> UpdateRange(IList<T> entities);

        bool SoftDelete(T entity);

        bool SoftDeleteRange(IList<T> entities);

        bool Delete(T entity);

        bool DeleteRange(IList<T> entities);

        int SaveChange();

        Task<int> SaveChangeAsync();
    }
}