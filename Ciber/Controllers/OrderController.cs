﻿using Microsoft.AspNetCore.Mvc;
using Ciber.Business.Interface;
using Ciber.Models;
using Ciber.Models.Request;
using Ciber.Providers;
using FromBodyAttribute = Microsoft.AspNetCore.Mvc.FromBodyAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using HttpPutAttribute = Microsoft.AspNetCore.Mvc.HttpPutAttribute;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using HttpDeleteAttribute = Microsoft.AspNetCore.Mvc.HttpDeleteAttribute;
using Microsoft.AspNetCore.Authorization;

namespace Ciber.Controllers
{
    //[Authorize]
    [Route("api/Order")]
    [ApiController]
    [AllowAnonymous]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _OrderService;
        private readonly IUserProvider _userProvider;

        public OrderController(IOrderService orderService, IUserProvider userProvider)
        {
            _OrderService = orderService;
            _userProvider = userProvider;
        }

        [Route("create")]
        [HttpPost]
        public IActionResult Save([FromBody] OrderModel model)
        {
            var currentUser = _userProvider.GetInfo();
            var response = _OrderService.Save(model, currentUser.Id);
            return Ok(response);
        }

        [Route("get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            var response = _OrderService.Get(id);
            return Ok(response);
        }

        [Route("list")]
        [HttpPost]
        public IActionResult List([FromBody] SearchModelParam<FilterProperty> searchModelParam)
        {
            var response = _OrderService.List(searchModelParam);
            return Ok(response);
        }
    }
}