﻿using Microsoft.AspNetCore.Mvc;
using Ciber.Business.Interface;
using Ciber.Models;
using Ciber.Models.Request;
using Ciber.Providers;
using FromBodyAttribute = Microsoft.AspNetCore.Mvc.FromBodyAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using HttpPutAttribute = Microsoft.AspNetCore.Mvc.HttpPutAttribute;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using HttpDeleteAttribute = Microsoft.AspNetCore.Mvc.HttpDeleteAttribute;
using Microsoft.AspNetCore.Authorization;
using Ciber.Business.Implement;

namespace Ciber.Controllers
{
    //[Authorize]
    [Route("api/category")]
    [ApiController]
    [AllowAnonymous]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        private readonly IUserProvider _userProvider;

        public CategoryController(ICategoryService categoryService, IUserProvider userProvider)
        {
            _categoryService = categoryService;
            _userProvider = userProvider;
        }

        [Route("create")]
        [HttpPost]
        public IActionResult Save([FromBody] CategoryModel model)
        {
            var currentUser = _userProvider.GetInfo();
            var response = _categoryService.Save(model, currentUser.Id);
            return Ok(response);
        }

        [Route("update")]
        [HttpPut]
        public IActionResult Update([FromBody] CategoryModel model)
        {
            var currentUser = _userProvider.GetInfo();
            var response = _categoryService.Update(model, currentUser.Id);
            return Ok(response);
        }

        [Route("get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            var response = _categoryService.Get(id);
            return Ok(response);
        }

        [Route("delete")]
        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var currentUser = _userProvider.GetInfo();
            var response = _categoryService.Delete(id, currentUser.Id);
            return Ok(response);
        }

        [Route("list")]
        [HttpPost]
        public IActionResult List([FromBody] SearchModelParam<FilterProperty> searchModelParam)
        {
            var response = _categoryService.List(searchModelParam);
            return Ok(response);
        }

        [Route("AllName")]
        [HttpGet]
        public IActionResult ListAll()
        {
            var response = _categoryService.ListAll();
            return Ok(response);
        }
    }
}