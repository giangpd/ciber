﻿using Microsoft.AspNetCore.Mvc;
using Ciber.Business.Interface;
using Ciber.Models;
using Ciber.Models.Request;
using Ciber.Providers;
using FromBodyAttribute = Microsoft.AspNetCore.Mvc.FromBodyAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using HttpPutAttribute = Microsoft.AspNetCore.Mvc.HttpPutAttribute;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using HttpDeleteAttribute = Microsoft.AspNetCore.Mvc.HttpDeleteAttribute;
using Microsoft.AspNetCore.Authorization;

namespace Ciber.Controllers
{
    //[Authorize]
    [Route("api/Product")]
    [ApiController]
    [AllowAnonymous]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [Route("AllName")]
        [HttpGet]
        public IActionResult ListAll(int categoryId)
        {
            var response = _productService.ListAll(categoryId);
            return Ok(response);
        }
    }
}