USE [master]
GO
/****** Object:  Database [Ciber]    Script Date: 3/10/2023 7:24:30 AM ******/
CREATE DATABASE [Ciber]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Ciber', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\Ciber.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Ciber_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\Ciber_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [Ciber] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Ciber].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Ciber] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Ciber] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Ciber] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Ciber] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Ciber] SET ARITHABORT OFF 
GO
ALTER DATABASE [Ciber] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Ciber] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Ciber] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Ciber] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Ciber] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Ciber] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Ciber] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Ciber] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Ciber] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Ciber] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Ciber] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Ciber] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Ciber] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Ciber] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Ciber] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Ciber] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Ciber] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Ciber] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Ciber] SET  MULTI_USER 
GO
ALTER DATABASE [Ciber] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Ciber] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Ciber] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Ciber] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Ciber] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Ciber] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [Ciber] SET QUERY_STORE = ON
GO
ALTER DATABASE [Ciber] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [Ciber]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 3/10/2023 7:24:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 3/10/2023 7:24:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 3/10/2023 7:24:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 3/10/2023 7:24:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 3/10/2023 7:24:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 3/10/2023 7:24:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 3/10/2023 7:24:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 3/10/2023 7:24:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 3/10/2023 7:24:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 3/10/2023 7:24:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 3/10/2023 7:24:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Amount] [int] NOT NULL,
	[OrderDate] [datetime2](7) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[CategoryName] [nvarchar](max) NOT NULL,
	[CustomerName] [nvarchar](max) NOT NULL,
	[ProductName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 3/10/2023 7:24:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Price] [real] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Quantity] [int] NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[IsDelete] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20230309021021_First', N'7.0.3')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20230309223155_Second', N'7.0.3')
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'47589937-012d-4b16-a55c-b7e2aa051943', N'giang', N'GIANG', N'giang@gmail.com', N'GIANG@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAELTmC5jp0vWLwn1L6ps8qtJTyGunpQN/4qpzIAKwRyih0mzJ18aYQsswrDJTltM5yA==', N'UM7SJN3IS6I5I7JTIAS2P67NQYMWICGY', N'fa5e3965-5807-4a31-846f-c0ad05e60d2a', NULL, 0, 0, NULL, 1, 0)
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([Id], [Name], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive]) VALUES (1, N'Quần áo', N'Cực nhiều quần áo', N'2104991b-1107-40af-a1cc-31c12c784c26', CAST(N'2023-03-09T02:13:31.8719022' AS DateTime2), N'2104991b-1107-40af-a1cc-31c12c784c26', CAST(N'2023-03-09T03:11:20.2459489' AS DateTime2), 0, 1)
INSERT [dbo].[Categories] ([Id], [Name], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive]) VALUES (2, N'Giày dép', N'Hàng hiệu', N'2d6c4262-f4df-4314-9557-b79e28777eaf', CAST(N'2023-03-09T03:26:49.7863459' AS DateTime2), NULL, CAST(N'2023-03-09T03:26:49.7865984' AS DateTime2), 0, 1)
INSERT [dbo].[Categories] ([Id], [Name], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive]) VALUES (3, N'Trang sức', N'đủ loại', N'fd6a6152-72c3-400e-a345-54e666fe141d', CAST(N'2023-03-09T05:14:01.8649587' AS DateTime2), NULL, CAST(N'2023-03-09T05:14:01.8655603' AS DateTime2), 0, 1)
INSERT [dbo].[Categories] ([Id], [Name], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive]) VALUES (4, N'Phụ kiện', N'đủ loại', N'fd6a6152-72c3-400e-a345-54e666fe141d', CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), NULL, CAST(N'2023-03-09T05:14:35.8274094' AS DateTime2), 0, 1)
INSERT [dbo].[Categories] ([Id], [Name], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive]) VALUES (5, N'Mũ', N'đẹp1', N'0f8693f9-8bf2-4cad-9e0c-5a3d301d1698', CAST(N'2023-03-09T05:15:38.6037086' AS DateTime2), NULL, CAST(N'2023-03-09T05:15:38.6039638' AS DateTime2), 0, 1)
INSERT [dbo].[Categories] ([Id], [Name], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive]) VALUES (6, N'Giầy', N'auth1', N'd4d69ffb-83cc-49ac-b22e-cf4d7d273382', CAST(N'2023-03-09T05:17:29.1616922' AS DateTime2), NULL, CAST(N'2023-03-09T05:17:29.1619066' AS DateTime2), 0, 1)
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([Id], [Name], [Address], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive]) VALUES (2, N'Đức Phúc', N'Quận 1', N'fd6a6152-72c3-400e-a345-54e666fe141d', CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), NULL, CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), 0, 1)
INSERT [dbo].[Customers] ([Id], [Name], [Address], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive]) VALUES (4, N'Erik', N'Quận 8', N'fd6a6152-72c3-400e-a345-54e666fe141d', CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), NULL, CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), 0, 1)
INSERT [dbo].[Customers] ([Id], [Name], [Address], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive]) VALUES (5, N'Hòa Minzy', N'Quận 9', N'fd6a6152-72c3-400e-a345-54e666fe141d', CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), NULL, CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), 0, 1)
SET IDENTITY_INSERT [dbo].[Customers] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([Id], [CustomerId], [ProductId], [Amount], [OrderDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive], [CategoryId], [CategoryName], [CustomerName], [ProductName]) VALUES (1, 2, 5, 43, CAST(N'2023-03-09T22:13:55.4560000' AS DateTime2), N'e07ff5dc-4d7f-4dff-9945-eadac87590c5', CAST(N'2023-03-09T22:23:05.5330418' AS DateTime2), NULL, CAST(N'2023-03-09T22:23:05.5334563' AS DateTime2), 0, 1, 1, N'Quần áo', N'Đức Phúc', N'Áo thun')
INSERT [dbo].[Orders] ([Id], [CustomerId], [ProductId], [Amount], [OrderDate], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive], [CategoryId], [CategoryName], [CustomerName], [ProductName]) VALUES (2, 2, 8, 94, CAST(N'2023-03-09T22:13:55.4560000' AS DateTime2), N'ce4575b9-b7f8-4322-82ea-a40f4b719997', CAST(N'2023-03-09T22:36:07.4275051' AS DateTime2), NULL, CAST(N'2023-03-09T22:36:07.4277421' AS DateTime2), 0, 1, 1, N'Quần áo', N'Đức Phúc', N'Áo khoác')
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([Id], [Name], [CategoryId], [Price], [Description], [Quantity], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive]) VALUES (5, N'Áo thun', 1, 12, N'a', 12, N'fd6a6152-72c3-400e-a345-54e666fe141d', CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), NULL, CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), 0, 1)
INSERT [dbo].[Products] ([Id], [Name], [CategoryId], [Price], [Description], [Quantity], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive]) VALUES (8, N'Áo khoác', 1, 43, N's', 4, N'fd6a6152-72c3-400e-a345-54e666fe141d', CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), NULL, CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), 0, 1)
INSERT [dbo].[Products] ([Id], [Name], [CategoryId], [Price], [Description], [Quantity], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDelete], [IsActive]) VALUES (9, N'Sneaker', 2, 3, N'r', 5, N'fd6a6152-72c3-400e-a345-54e666fe141d', CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), NULL, CAST(N'2023-03-09T05:18:35.8274032' AS DateTime2), 0, 1)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 3/10/2023 7:24:31 AM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 3/10/2023 7:24:31 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 3/10/2023 7:24:31 AM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 3/10/2023 7:24:31 AM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 3/10/2023 7:24:31 AM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 3/10/2023 7:24:31 AM ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 3/10/2023 7:24:31 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Orders] ADD  DEFAULT ((0)) FOR [CategoryId]
GO
ALTER TABLE [dbo].[Orders] ADD  DEFAULT (N'') FOR [CategoryName]
GO
ALTER TABLE [dbo].[Orders] ADD  DEFAULT (N'') FOR [CustomerName]
GO
ALTER TABLE [dbo].[Orders] ADD  DEFAULT (N'') FOR [ProductName]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
USE [master]
GO
ALTER DATABASE [Ciber] SET  READ_WRITE 
GO
