﻿namespace Ciber.Common.Helpers
{
    public static class AppHelper
    {
        public static string CombineTimeToDigitFormat(DateTime dateTime, string format)
        {
            try
            {
                return dateTime.ToString(format);
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}