﻿using Newtonsoft.Json;

namespace Ciber.Common.Helpers
{
    public static class ConvertorHandler
    {
        public static T ConvertValueToDestination<T>(this object value)
        {
            try
            {
                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch (Exception)
            {
                return default;
            }
        }

        public static T? PreprocessResponse<T>(this string? jsonData)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonData);
            }
            catch (Exception)
            {
                return default;
            }
        }
    }
}