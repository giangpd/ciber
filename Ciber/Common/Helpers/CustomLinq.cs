﻿using Ciber.Extensions;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Dynamic.Core;

namespace Ciber.Common.Helpers
{
    public static class CustomLinq
    {
        public static IQueryable<T> PageBy<T>([NotNull] this IQueryable<T> query, IPagedAndSortedRequest pagedResultRequest)
        {
            var skipCount = (pagedResultRequest.PageIndex - 1) * pagedResultRequest.PageSize;
            return query.PageBy(skipCount, pagedResultRequest.PageSize);
        }

        public static IQueryable<T> OrderBy<T>([NotNull] this IQueryable<T> query, IPagedAndSortedRequest pagedResultRequest)
        {
            var sorting = pagedResultRequest.SortBy + " " + pagedResultRequest.SortDirection;
            return query.OrderBy(sorting);
        }

        public static IQueryable<T> Contains<T>([NotNull] this IQueryable<T> query, string property, string value)
        {
            var contains = $"{property}.Contains(@0)";
            return query.Where(contains, value);
        }
    }
}