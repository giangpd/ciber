﻿namespace Ciber.Common.Helpers
{
    public class BaseResponse
    {
        public bool Success { get; set; } = false;
        public string Message { get; set; }
        public int StatusCode { get; set; }

        public BaseResponse()
        {
        }

        public BaseResponse(bool success, string message, int statusCode)
        {
            Success = success;
            StatusCode = statusCode;
            Message = message;
        }

        public BaseResponse(bool success, int statusCode)
        {
            Success = success;
            StatusCode = statusCode;
        }
    }

    public class Response<T> : BaseResponse
    {
        public T Data { get; set; }
        public int Total { get; set; }

        public Response()
        {
        }

        public Response(T data, bool success)
        {
            Data = data;
            Success = success;
        }

        public Response(T data, bool success, int statusCode)
        {
            Data = data;
            StatusCode = statusCode;
            Success = success;
        }

        public Response(bool success, string message, int statusCode)
        {
            Success = success;
            StatusCode = statusCode;
            Message = message;
        }

        public Response(T data, bool success, string message, int statusCode)
        {
            Data = data;
            Success = success;
            StatusCode = statusCode;
            Message = message;
        }

        public Response(T data, bool success, int total, int statusCode)
        {
            Data = data;
            Success = success;
            Total = total;
            StatusCode = statusCode;
        }
    }
}