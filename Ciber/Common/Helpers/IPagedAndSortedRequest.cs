﻿namespace Ciber.Common.Helpers
{
    public interface IPagedAndSortedRequest
    {
        int PageIndex { get; set; }
        int PageSize { get; set; }
        string SortBy { get; set; }
        string SortDirection { get; set; }
    }

    public class PagedAndSortedRequest
    {
        public PagedAndSortedRequest()
        {

        }
        public PagedAndSortedRequest(int pIndex, int pSize, string sortBy, string sortDirection)
        {
            PageIndex = pIndex;
            PageSize = pSize;
            SortBy = sortBy;
            SortDirection = sortDirection;
        }

        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 5;
        public string SortBy { get; set; } = "CreatedDate";
        public string SortDirection { get; set; } = "Desc";
    }
}