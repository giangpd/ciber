﻿namespace Ciber.Common.Helpers
{
    public static class ObjectStateHelper
    {
        public static bool IsNull<T>(this T dest) => dest is null;

        public static bool IsNotNull<T>(this T dest) => !(dest is null);

        public static bool IsNullOrEmpty<T>(this List<T> dest)
        {
            if (dest.IsNull())
                return true;
            else if (!dest.Any())
                return true;

            return false;
        }

        public static T DefaultGenValue<T>() => default;
    }
}