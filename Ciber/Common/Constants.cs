﻿namespace Ciber.Common
{
    public static class Constants
    {
        public static string PrefixTableName = "AspNet";
        public static string BearerToken = "Bearer";

        public static class DatetimeDigitFormat
        {
            /// <summary>
            /// 05:50
            /// </summary>
            public static string HHmm = "HHmm";

            /// <summary>
            /// 05:50 AM
            /// </summary>
            public static string hhmmtt = "hhmmtt";

            /// <summary>
            /// 5:50
            /// </summary>
            public static string Hmm = "Hmm";

            /// <summary>
            /// 5:50 AM
            /// </summary>
            public static string hmmtt = "hmmtt";

            /// <summary>
            /// 05:50:06
            /// </summary>
            public static string HHmmss = "HHmmss";

            /// <summary>
            /// 2015 May
            /// </summary>

            public static string yyyyMMMM = "yyyyMMMM";
        }

        public static class ApiResponseMessage
        {
            public static string CreateOk = "Create successfully";
            public static string UpdateOk = "Update successfully";
            public static string DeleteOk = "Delete successfully";
            public static string MediaStopped = "Media stopped";
            public static string DuplicatePlay = "Duplicate playing";

            public static string NotFound = "Not found";
        }

        public static class CustomClaims
        {
            public const string Permissions = "Permissions";
        }
    }
}