﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Ciber.Infrastructure.Entity
{
    public class Category : FullAuditedEntity
    {
        public string Name { get; set; }
        public string? Description { get; set; }
    }
}
