﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Ciber.Infrastructure.Entity
{
    public class Product :FullAuditedEntity
    {
        public string Name { get; set; }

        public int CategoryId { get; set; }

        public float Price { get; set; } = 0.00f;

        public string Description { get; set; }

        public int Quantity { get; set; } = 0;
    }
}
