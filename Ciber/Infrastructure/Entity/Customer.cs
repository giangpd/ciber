﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Ciber.Infrastructure.Entity
{
    public class Customer : FullAuditedEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
