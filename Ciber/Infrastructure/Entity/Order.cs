﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Ciber.Infrastructure.Entity
{
    public class Order : FullAuditedEntity
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }

        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public int Amount { get; set; } = 0;

        public DateTime OrderDate { get; set; } = DateTime.Now;
    }
}
