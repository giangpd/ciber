﻿using Ciber.Common;
using Ciber.Infrastructure.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ciber.Infrastructure.Configurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.CustomerId).IsRequired();
            builder.Property(x => x.CustomerName);
            builder.Property(x => x.ProductId).IsRequired();
            builder.Property(x => x.ProductName);
            builder.Property(x => x.CategoryId).IsRequired();
            builder.Property(x => x.CategoryName);
            builder.Property(x => x.Amount).IsRequired();
            builder.Property(x => x.OrderDate);
            builder.Property(x => x.Id).UseIdentityColumn();

            builder.ToTable("Orders");
        }
    }
}