﻿using Microsoft.AspNetCore.Identity;

namespace Ciber.Providers
{
    public interface IUserProvider
    {
        public IdentityUser GetInfo();
    }

    public class UserProvider : IUserProvider
    {
        private static readonly object Locker = new object();
        private IdentityUser LoggedOnUser { get; set; }

        public UserProvider(IdentityUser user)
        {
            LoggedOnUser = user;
        }

        public IdentityUser GetInfo()
        {
            return LoggedOnUser;
        }
    }
}