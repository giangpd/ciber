﻿namespace Ciber.Initial.Ioc
{
    public static class IoC
    {
        public static void Register(IServiceCollection services)
        {
            ServiceInitiator.Register(services);
            AutoMapperInitiator.Register(services);
        }
    }
}