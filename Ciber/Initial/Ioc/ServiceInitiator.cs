﻿using Ciber.Business.Implement;
using Ciber.Business.Interface;
using Ciber.Repositories.Implement;
using Ciber.Repositories.Interface;

namespace Ciber.Initial.Ioc
{
    public static class ServiceInitiator
    {
        public static void Register(IServiceCollection services)
        {
            services.AddHttpContextAccessor();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            #region [Repositories]

            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IUserRepository, UserRepository>();

            #endregion [Repositories]

            #region [Services]

            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IProductService, ProductService>();

            #endregion [Services]
        }
    }
}