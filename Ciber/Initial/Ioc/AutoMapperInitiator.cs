﻿using AutoMapper;
using Ciber.Initial.Mapper;

namespace Ciber.Initial.Ioc
{
    public static class AutoMapperInitiator
    {
        public static void Register(IServiceCollection services)
        {
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MapperProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
