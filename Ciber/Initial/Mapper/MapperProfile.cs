﻿using AutoMapper;
using Ciber.Infrastructure.Entity;
using Ciber.Models.Request;

namespace Ciber.Initial.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<CategoryModel, Category>();
            CreateMap<Category, CategoryModel>();
            CreateMap<Category, CategoryResponse>();
            CreateMap<OrderModel, Order>();
            CreateMap<Order, OrderModel>();
            CreateMap<Product, ProductResponse>();
        }
    }
}